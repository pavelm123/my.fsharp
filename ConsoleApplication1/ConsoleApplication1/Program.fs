﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.
(*
multi-line comments
*)
let pi = 3.1415926

// ======== Lists ============
let twoToFive = [2;3;4;5]        // Square brackets create a list with
                                 // semicolon delimiters.
let oneToFive = 1 :: twoToFive   // :: creates list with new 1st element
// The result is [1;2;3;4;5]
let zeroToFive = [0;1] @ twoToFive   // @ concats two lists

// ======== Functions ========
// The "let" keyword also defines a named function.
let square x = x * x          // Note that no parens are used.
let sq = square 3             // Now run the function. Again, no parens.

let add x y = x + y           // don't use add (x,y)! It means something
                              // completely different.
let a = add 2 3               // Now run the function.
// to define a multiline function, just use indents. No semicolons needed.
let evens list =
   let isEven x = x%2 = 0     // Define "isEven" as a sub function
   List.filter isEven list    // List.filter is a library function
                              // with two parameters: a boolean function
                              // and a list to work on

evens oneToFive               // Now run the function

// You can use parens to clarify precedence. In this example,
// do "map" first, with two args, then do "sum" on the result.
// Without the parens, "List.map" would be passed as an arg to List.sum
let sumOfSquaresTo100 =
    List.sum ( List.map square [1..100] )

// кстати, все работает с подсказкой: List. выдает огромную тучу методов


// You can pipe the output of one operation to the next using "|>" - ЭТО PIPE
// Here is the same sumOfSquares function written using pipes
let sumOfSquaresTo100piped =
   [1..100] |> List.map square |> List.sum  // "square" was defined earlier
// you can define lambdas (anonymous functions) using the "fun" keyword
let sumOfSquaresTo100withFun =
   [1..100] |> List.map (fun x->x*x) |> List.sum

[<EntryPoint>]
let main argv = 
    printfn "%A %A" argv sumOfSquaresTo100piped  // почему-то выводит в виде: [|"123"|] 3.141593, где 123 - параметр...
    0 // return an integer exit code


// In F# there is no "return" keyword. A function always
// returns the value of the last expression used.


